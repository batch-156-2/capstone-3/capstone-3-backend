// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/orders');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

route.get('/cart', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let id = payload.id;
	let isAdmin = payload.isAdmin;
	let data = {
		userId: id
	}
	if (!isAdmin) {
		cont.getOrder(data).then(outcome => {
			res.send(outcome);
		})
	} else {
		res.send('User is an admin. Access denied.')
	}
});

// Exports
module.exports = route;