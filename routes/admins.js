// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/admins');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

// Post Route
route.post('/newproduct', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	let data = {
		product: req.body
	};
	if (isAdmin) {
		cont.addProduct(data).then(outcome => {
			res.send(outcome)
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
});

// Get Route
route.get('/all', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		cont.getAllProducts().then(outcome => {
			res.send(outcome)
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
});

// Put Route
route.put('/:productId', auth.verify, (req, res) => {
	let params = req.params;
	let body = req.body;
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		cont.updateProduct(params, body).then(outcome => {
			res.send(outcome)
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
});

// Product Status
route.put('/:productId/setActive', (req, res) => {
	let productId = req.params.productId
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		cont.setAsActive(productId).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
});
 
route.put('/:productId/archive', auth.verify, (req, res) => {
	let productId = req.params.productId
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		cont.archiveProduct(productId).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
})

// Delete Route
route.delete('/:productId/delete', auth.verify, (req, res) => {
	let productId = req.params.productId
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	if (isAdmin) {
		cont.deleteProduct(productId).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('Unauthorized action. User not an admin.')
	}
});

// Exports
module.exports = route;
