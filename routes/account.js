// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/account');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

// Post Route
route.post('/register', (req, res) => {
	let userData = req.body;
	cont.register(userData).then(result => {
		res.send(result);
	});
});

route.post('/login', (req, res) => {
	let data = req.body;
	cont.loginAccount(data).then(outcome => {
		res.send(outcome);
	});
});

// Get Route
route.get('/user', auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	let userId = userData.id;
	cont.getProfile(userId).then(outcome => {
		res.send(outcome);
	})
});

// Put Route
route.put('/update', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	cont.updateAccount(req.body).then(outcome => {
		res.send(outcome);
	});
});

// Exports
module.exports = route;