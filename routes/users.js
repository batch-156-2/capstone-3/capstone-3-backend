// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/users');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

// Post Route
route.post('/purchase', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let id = payload.id;
	let isAdmin = payload.isAdmin;
	let prdctName = req.body.productName;
	let prdctId = req.body.productId;
	let pPrice = req.body.productPrice;
	let prdctQtty = req.body.quantity;
	let data = {
		userId: id,
		productId: prdctId,
		productName: prdctName,
		productPrice: pPrice,
		productQuantity: prdctQtty
	}
	if (!isAdmin) {
		cont.purchase(data).then(outcome => {
			res.send(outcome);
		})
	} else {
		res.send('User is an admin. Purchase failed.');
	};
});

// Get Route
route.get('/active', (req, res) => {
	cont.getAllActive().then(outcome => {
		res.send(outcome);
	});
});

route.get('/:productId', (req, res) => {
	let productId = req.params.productId;
	cont.getProduct(productId).then(outcome => {
		res.send(outcome);
	});
});

// Exports
module.exports = route;