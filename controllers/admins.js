// Dependencies and Modules
const Product = require('../models/Product');

// Create Functionality
module.exports.addProduct = (info) => {
	let product = info.product
	let pName = product.name;
	let pPrice = product.price;
	let pImage = product.image;
	let newProduct = new Product({
		name: pName,
		price: pPrice,
		image: pImage
	});
	return newProduct.save().then((saved, err) => {
		if (saved) {
			return saved;
		} else {
			return false;
		}
	});
}

// Retrieve Functionality
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

// Update Functionality
module.exports.updateProduct = (product, details) => {
	let pName = details.name;
	let pPrice = details.price;
	let pImage = details.image;
	let updatedProduct = {
		name: pName,
		price: pPrice,
		image: pImage
	};
	let id = product.productId;
	return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
		if (productUpdated) {
			return productUpdated;
		} else {
			return 'Failed to update.'
		};
	});
};


// Product Status
module.exports.setAsActive = (productId) => {
	let updates = {
		isActive: true
	}
	return Product.findByIdAndUpdate(productId, updates).then((productActive, err) => {
		if (productActive) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.archiveProduct = (productId) => {
	let update = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productId, update).then((archived, err) => {
		if (archived) {
			return true;
		} else {
			return false;
		}
	})
}

// Delete Functionality
module.exports.deleteProduct = (productId) => {
	return Product.findById(productId).then(product => {
		if (product === null) {
			return 'No product found.'
		} else {
			return product.remove().then((removedProduct, err) => {
				if (err) {
					return 'Failed to remove.'
				} else {
					return 'Successfully removed.'
				}
			});
		};
	});
};

