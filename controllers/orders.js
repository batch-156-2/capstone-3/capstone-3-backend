// Dependencies and Modules
const Order = require('../models/Order');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.getOrder = (data) => {
	let totalData = {}
	let buyerId = data.userId;
	return Order.find({userId: buyerId}).then(orders => {
		totalData.orders = orders;
		let totalOrderPrice = 0

		// Get summation of all orders.
		for (let i = 0; i < orders.length; i++) {
			totalOrderPrice += orders[i].totalPrice;
		}

		totalData.totalOrderPrice = totalOrderPrice
		return totalData;
	})
};