// Dependencies and Modules
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Create Functionality
module.exports.purchase = (data) => {
	let id = data.userId;
	let prdctId = data.productId;
	let prdctQtty = data.productQuantity;
	return Product.find({_id: prdctId}).then(result => {
		if (result.length > 0) {
			let newOrder = new Order({
				userId: id,
				productId: result[0]._id,
				productName: result[0].name,
				quantity: prdctQtty,
				totalPrice: parseInt(prdctQtty) * parseInt(result[0].price)
			})
			return newOrder.save().then((order, err) => {
				if (order) {
					return order;
				} else {
					return 'Error.';
				};
			});
		} else {
			return 'No products found.'
		};
	});
};

// Retrieve Functionality
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

module.exports.getProduct = (id) => {
	return Product.findById(id).then(result => {
		return result;
	});
};
