// Dependencies and Modules
const Account = require('../models/Account');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// Create Functionality
module.exports.register = (reqBody) => {
	let ema = reqBody.email;
	let fN = reqBody.firstName;
	let mN = reqBody.middleName;
	let lN = reqBody.lastName;
	let pw = reqBody.password;
	let mobileNo = reqBody.mobileNumber;
	return Account.find({email: ema}).then(result => {
		if (result.length === 0) { //If email does not exist, register user.
			let newAccount = new Account({
				firstName: fN,
				middleName: mN,
				lastName: lN,
				email: ema,
				password: bcrypt.hashSync(pw, 10),
				mobileNumber: mobileNo
			});
			return newAccount.save().then((saved, err) => {
				if (saved) {
					return saved
				} else {
					return 'Error.'
				};
			});
		} else {
			return 'Email already exists.'
		}
	})
};

module.exports.loginAccount = (reqBody) => {
	return Account.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			return false;
		} else {
			const isMatched = bcrypt.compareSync(reqBody.password, result.password);
			if (isMatched) {
				let dataUser = result.toObject();
				return {accessToken: auth.createAccessToken(dataUser)}
			} else {
				return false;
			}
		};
	});
};

// Retrieve Functionality
module.exports.getProfile = (userId) => {
	return Account.findById(userId).then(user => {
		return user;
	});
};

// Update Functionality
module.exports.updateAccount = (reqBody) => {
	let newPw = reqBody.password;
	let email = reqBody.email;
	let fN = reqBody.firstName;
	let mN = reqBody.middleName;
	let lN = reqBody.lastName;
	let mobileNo = reqBody.mobileNumber;
	return Account.find({email: email}).then((foundAccount, err) => {
		if (foundAccount.length > 0) {
			let updates = {
				password: bcrypt.hashSync(newPw, 10),
				email: email,
				firstName: fN,
				middleName: mN,
				lastName: lN,
				mobileNumber: mobileNo
			}
			return Account.findByIdAndUpdate(foundAccount[0]._id, updates).then((update, err) => {
				if (err) {
					return 'Error updating.'
				} else {
					return 'Account successfully updated.'
				}
			});
		} else {
			return 'No account found.'
		};
	});
};