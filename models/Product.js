// Dependencies and Modules
const mongoose = require('mongoose');

// Schema
const productSchema = new mongoose.Schema({
	image: {
		type: String,
		required: [true, 'Image is required']
	},
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});	

// Model
module.exports = mongoose.model("Product", productSchema);