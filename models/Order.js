// Dependencies and Modules
const mongoose = require('mongoose');

// Schema
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required.']
	},
	productName: {
		type: String,
		required: [true, 'Product name is required.']
	},
	productId: {
		type: String,
		required: [true, 'Product ID is required.']
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	quantity: {
		type: Number
	},
	totalPrice: {
		type: Number
	}
});

// Model
module.exports = mongoose.model("Order", orderSchema)