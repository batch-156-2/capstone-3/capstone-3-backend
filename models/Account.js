// Dependencies and Modules
const mongoose = require('mongoose');

// Schema
const accountSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	middleName: {
		type: String,
		required: [true, 'Middle name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	mobileNumber: {
		type: String,
		required: [true, 'Mobile Number is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

// Model
module.exports = mongoose.model("Account", accountSchema)