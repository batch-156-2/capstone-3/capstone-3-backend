// Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const accountRoutes = require('./routes/account');
	const adminRoutes = require('./routes/admins');
	const userRoutes = require('./routes/users');
	const orderRoutes = require('./routes/orders');

// Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const mu = process.env.MONGO_URL;

// Server Setup
	const app = express();
	app.use(cors());
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

// Server Routes
	app.use('/account', accountRoutes);
	app.use('/admin', adminRoutes);
	app.use('/user', userRoutes);
	app.use('/order', orderRoutes);

// Database Connect
	mongoose.connect(mu);
	const db = mongoose.connection;
	db.once('open', () => console.log(`Connected to Atlas!`));

// Server Response
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});